﻿using UnityEngine;
using System.Collections;


namespace CoAHomework
{
    public class Addition : MonoBehaviour
    {   
        // Alle Variablen, von denen gefühlt nur 2% genutzt wurden 
    
        public int sum;
        public int numberOne = 8;
        public int numberTwo = 2;
        public int numberThree = 5;
        public int numberFour = 5;
        public int numberFive = 8;
        public int numberSix = 87;
      

        public void Start()
        {
         // Nur mit dem If-Statement hat es ""geklappt"", zwei Rechnungen mit int sum als Summe zu machen, allerdings ist das anscheinend falsch.
         // Und der Debug.Log funktioniert nicht?

         Debug.Log("Hallo, ich bin ein passiv-aggressiver Debug.Log, der nicht funktioniert :)");

            if (numberThree != 5)
            {
                sum = numberOne + numberTwo;
                Debug.Log("Dieses if-statement ergibt viel Sinn!" + numberOne);
            }

            else
            {
                sum = numberThree + numberFour;
                Debug.Log("Ich weiß zum Glück absolut, was ich hier gerade tue" + numberThree);
            }
        }
    }
}

// My last 2 braincells doing this assignment (and not being able to finish or understand most of it):
// ░░░░░░░░███████████████░░░░░░░░
// ░░░░░█████████████████████░░░░░
// ░░░░████████████████████████░░░
// ░░░██████████████████████████░░
// ░░█████████████████████████████
// ░░███████████▀░░░░░░░░░████████
// ░░███████████░░░░░░░░░░░░░░░███
// ░████████████░░░░░░░░░░░░░░░░██
// ░█░░███████░░░░░░░░░░░▄▄░░░░░██
// █░░░░█████░░░░░░▄███████░░██░░█
// █░░█░░░███░░░░░██▀▀░░░░░░░░██░█
// █░░░█░░░░░░░░░░░░▄██▄░░░░░░░███
// █░░▄█░░░░░░░░░░░░░░░░░░█▀▀█▄░██
// █░░░░░░░░░░░░░░░░░░░░░░█░░░░██░
// ░███░░░░░░░░░░░░░░░░░░░█░░░░█░░
// ░░█░█░░░░░░░█░░░░░██▀▄░▄██░░░█░
// ░░█░█░░░░░░█░░░░░░░░░░░░░░░░░█░
// ░░░██░░░░░░█░░░░▄▄▄▄▄▄░░░░░░█░░
// ░░░██░░░░░░░█░░█▄▄▄▄░▀▀██░░█░░░
// ░░░██░░░░░░░█░░▀████████░░█░░░░
// ░░█░░█░░░░░░░█░░▀▄▄▄▄██░░█░░░░░
// ░░█░░░█░░░░░░░█░░░░░░░░░█░░░░░░
// ░█░░░░░█░░░░░░░░░░░░░░░░█░░░░░░
// ░░░░░░░░█░░░░░░█░░░░░░░░█░░░░░░
// ░░░░░░░░░░░░░░░░████████░░░░░░░
